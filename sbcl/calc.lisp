(defvar a)
(defvar b)
(defvar c)

(write-line "Enter value of a and b")

(setf a(read))
(setf b(read))

(sb-thread:make-thread (lambda()
		       (progn
			(setf c(+ a b))
			(print "Addition is: ")
			(format t "~b" c)
			(print "Addition in decimal: ")
			(print c))))
(sb-thread:make-thread (lambda()
		       (progn
			(setf c(- a b))
			(print "sub is: ")
			(format t "~b" c)
			(print "sub in decimal: ")
			(print c))))
(sb-thread:make-thread (lambda()
		       (progn
			(setf c(/ a b))
			(print "Div is: ")
			(format t "~b" c)
			(print "Div in decimal: ")
			(print c))))

(sb-thread:make-thread (lambda()
		       (progn
			(setf c(* a b))
			(print "mul is: ")
			(format t "~b" c)
			(print "mul in decimal: ")
			(print c))))

(sb-thread:make-thread (lambda()
		       (progn
			(setf c(sin a))
			(print "sin is: ")
			(format t "~b" c)
			(print "sin in decimal: ")
			(print c))))
