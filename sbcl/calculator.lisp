(defvar a)
(defvar b)
(defvar c)

(write-line "Enter the value of a and b")

(setf a(read))
(setf b(read))

(sb-thread:make-thread  (lambda()
			(progn
			(setf c(+ a b))
			(print "Addition in binary")
			(format t "~b" c)
			(print "addition in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			(setf c(- a b))
			(print "substraction in binary")
			(format t "~b" c)
			(print "substraction in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			(setf c(* a b))
			(print "multiplication in binary")
			(format t "~b" c)
			(print "multiplication in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			
			(setf c(/ a b))
			(print "division in binary")
			(format t "~b" c)
			(print "divsion in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			
			(setf c(* a a))
			(print "square in binary")
			(format t "~b" c)
			(print "square in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			
			(setf c(* b b))
			(print "square in binary")
			(format t "~b" c)
			(print "square in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			
			(setf c(min a b))
			(print "min in binary")
			(format t "~b" c)
			(print "min in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			(setf c(max a b))
			(print "max in binary")
			(format t "~b" c)
			(print "max in decimal")
			(print c))))

(sb-thread:make-thread  (lambda()
			(progn
			(setf c(sin a ))
			(print "sin in binary")
			(format t "~b" c)
			(print "sin in decimal")
			(print c))))

(sb-thread:make-thread(lambda()
			(progn
			(setf c(cos a))
			(print "cos in binary")
			(format t "~b" c)
			(print "cos in decimal")
			(print c))))
