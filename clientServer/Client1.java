import java.net.*;
import java.io.*;
public class Client1
{
	public static void main(String args[])throws IOException
	{
		Socket clientsocket=new Socket("localhost",9898);
		System.out.println("connected to server");

		PrintWriter out = new PrintWriter(clientsocket.getOutputStream(),true) ;
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		String response;
		BufferedReader br1=new BufferedReader(new InputStreamReader(clientsocket.getInputStream()));

		while((response=br.readLine())!=null){
			out.println(response);
			response=br1.readLine();

			if (response == null || response.equals("")) {
                          System.exit(0);
			}
			System.out.println("Echo:"+response);
		}
		out.close();
		br1.close();
		clientsocket.close();
	}
}
