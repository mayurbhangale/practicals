import java.net.*;
import java.io.*;

public class Server1 extends Thread
{
 protected Socket clntSock;
public static void main(String args[])throws IOException
{
ServerSocket serSock=new ServerSocket(9898);
while (true)
                 {
                  System.out.println ("Waiting for Connection");
                  new Server1 (serSock.accept()); 
                 }
}
private Server1 (Socket clientSoc)
{
    clntSock = clientSoc;
    start();
}
public void run()
{
    System.out.println ("New Communication Thread Started");

	try
	{
	BufferedReader in = new BufferedReader(new InputStreamReader(clntSock.getInputStream())) ;
	String m;
	PrintWriter sout=new PrintWriter(clntSock.getOutputStream(),true);
	while((m=in.readLine())!=null)
	{
		System.out.println("\nServer:"+m);
		sout.println(m);
		if (m.equalsIgnoreCase("Bye")) 
            	break; 
	}
	sout.close();
	in.close();
	clntSock.close(); 
}
catch (IOException e) 
        { 
         System.err.println("Problem with Communication Server");
         System.exit(1); 
        } 
}
}

