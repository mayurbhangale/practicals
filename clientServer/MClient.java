import java.net.*;
import java.io.*;

class MClient {
	public static void main(String args[]) throws IOException{
		
		Socket msock = null;
		DataOutputStream os = null;
		BufferedReader is = null;

		msock = new Socket("localhost", 6787);
		
		os = new DataOutputStream(msock.getOutputStream());
		is = new BufferedReader(new InputStreamReader(msock.getInputStream()));
		
		while(true){
			System.out.println("Enter integer: ");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String input = reader.readLine();
			
			os.writeBytes(input + "\n");
			
			String response = is.readLine();
			System.out.println("Server returns square as: "+response);
		}

		os.close();
		is.close();
		msock.close();
	}
}

